# Desenvolvedor front-end/mobile na Vibn

Objetivo deste desafio é avaliarmos o seu domínio em front-end/mobile, ou seja, sua organização, estilo e boas práticas com o código, conhecimento dos frameworks e tecnologias utilizadas.

---

## Regras

1.  Todo o seu código deve ser disponibilizado num repositório público ou privado em seu Github ou Bitbucket pessoal. Envie o link para [rodrigodugin@gmail.com](rodrigodugin@gmail.com);
2.  Desenvolver o projeto utilizando create react native app ou react native cli
3.  Escrever um README com instruções para rodar o projeto

---

## O desafio

O desafio consiste em desenvolver um aplicativo que contém duas telas consumindo uma API.

**obs:** Você tem liberdade total para criar uma interface que preferir, levando em consideração os requisitos abaixo.

#### Primeira Tela

1.  Uma lista com **75 cartas** recebidas **aleatoriamente** da API
2.  Mostrar quantas cartas foram recebidas da API e quantas são no total. Ex: 75 de N ( onde N é o total de cartas existentes na API )

#### Segunda Tela

1.  Mostrar os detalhes dessa carta contendo:

    * nome
    * custo de mana
    * cores
    * tipo
    * texto
    * poder, resistência e/ou lealdade (se existir)

2.  Se quiser acrescentar mais informações, fique a vontade.

#### API

* a API utilizada no desafio será a seguinte: https://docs.magicthegathering.io/

---

## Dúvidas

Envie suas dúvidas diretamente para [rodrigodugin@gmail.com](rodrigodugin@gmail.com)
